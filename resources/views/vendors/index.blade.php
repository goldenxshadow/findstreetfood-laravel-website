@extends('layouts.mainlayout')

@section('content')
    <h1> Vendors </h1>
    
    @if(count($vendors) > 0)
        @foreach($vendors as $vendor)
            <div class="well">
            <h3><a href="/vendors/{{$vendor->id}}">{{$vendor->name}}</a></h3>
            </div>
        @endforeach
    @else
        <p>No vendors found </p>
    @endif

@endsection