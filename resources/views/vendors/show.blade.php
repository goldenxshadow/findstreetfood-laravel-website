@extends('layouts.mainlayout')

@section('title')
Vendor
@endsection

@section('content')
@php
//Address
$streetaddress = $vendor->vendorlocation->streetaddress;
$postcode = $vendor->vendorlocation->postcode;
$country = $vendor->vendorlocation->country;
$city = $vendor->vendorlocation->city;
$fulladdress = $streetaddress . ", " . $postcode . ", " . $city . ", " . $country;

//Number
$number = $vendor->number;


@endphp
<!--Page Wrapper starts-->
<div class="page-wrapper fixed-footer">
    <!--Listing Details Hero starts-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-details-title v2">
                    <div class="row">
                        <div class="col-lg-6 col-md-7 col-sm-12">
                            <div class="single-listing-title float-left">
                                <p><a href="#" class="btn v6 red">Vendor</a></p>
                                <h2>{{$vendor->name}}<i class="icofont-tick-boxed"></i></h2>
                                <p><i class="ion-ios-location"></i> {{$vendor->vendorlocation->country}}</p>
                                <div class="list-ratings">
                                    <span class="ion-android-star"></span>
                                    <span class="ion-android-star"></span>
                                    <span class="ion-android-star"></span>
                                    <span class="ion-android-star"></span>
                                    <span class="ion-android-star-half"></span>
                                    <a href="#">(xyz Reviews)</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5 col-sm-12">
                            <div class="list-details-btn text-right sm-left">
                                <div class="save-btn">
                                    <a href="#" class="btn v3"><i class="ion-heart"></i> Save</a>
                                </div>
                                <div class="share-btn">
                                    <a href="#" class="btn v3"><i class="ion-android-share-alt"></i> Share</a>
                                    <ul class="social-share">
                                        <li class="bg-fb"><a href="#"><i class="ion-social-facebook"></i></a></li>
                                        <li class="bg-tt"><a href="#"><i class="ion-social-twitter"></i></a></li>
                                        <li class="bg-ig"><a href="#"><i class="ion-social-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Hero ends-->
    <!--Listing Details Info starts-->
    <div class="list-details-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <!--Listing Details starts-->
                    <div class="list-details-wrap">
                        <div id="overview" class="list-details-section">
                            <h4>Overview</h4>
                            <div class="overview-content">
                                <p class="mar-bot-10">{{$vendor->about}}</p>
                            </div>

                            @php
                            //Creating an array for each vendorattribite type and filling it with the respecitve
                            //attributes. Used in the 'overview' section
                            foreach ($vendor->vendorattributes as $vendorattribute){
                            if($vendorattribute->type == 0){

                            $amenities = [];
                            $amenities[] = $vendorattribute->attribute;

                            }
                            if($vendorattribute->type == 1){

                            $cuisines= [];
                            $cuisines[] = $vendorattribute->attribute;

                            }
                            if($vendorattribute->type == 2){

                            $meals = [];
                            $meals[] = $vendorattribute->attribute;

                            }
                            if($vendorattribute->type == 4){

                            $diets = [];
                            $diets[] = $vendorattribute->attribute;

                            }
                            }

                            @endphp

                            @if(count($amenities) > 0)
                            <div class="mar-top-20">
                                <h6>Features and Amenities</h6>
                                <ul class="listing-features">
                                    @foreach ($amenities as $amentitie)
                                    <li> {{$amentitie}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            @if(count($cuisines) > 0)
                            <div class="mar-top-20">
                                <h6>Cuisines</h6>
                                <ul class="listing-features">
                                    @foreach ($cuisines as $cuisine)
                                    <li> {{$cuisine ?? ''}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            @if(count($meals) > 0)
                            <div class="mar-top-20">
                                <h6>Meals</h6>
                                <ul class="listing-features">
                                    @foreach ($meals as $meal)
                                    <li> {{$meal}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            @if(count($diets) > 0)
                            <div class="mar-top-20">
                                <h6>Diets</h6>
                                <ul class="listing-features">
                                    @foreach ($diets as $diet)
                                    <li> {{$diet}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
{{-- 

                            <div class="mar-top-20">
                                <h6>Spechial diets</h6>
                                <ul class="listing-features">
                                    <li><i class="icofont-credit-card"></i> Accepts Credit cards</li>
                                    <li><i class="icofont-no-smoking"></i> No Smoking zone</li>
                                    <li><i class="icofont-car-alt-3"></i> Free Parking on premises</li>
                                    <li><i class="icofont-snow"></i>Air Conditioned</li>
                                    <li><i class="icofont-search-restaurant"></i> Online order</li>
                                    <li><i class="icofont-wheelchair"></i> Wheelchair Friendly</li>
                                    <li><i class="icofont-paw"></i> Pet Friendly </li>
                                </ul>
                            </div>
                        </div> --}}


                        <div id="gallery" class="list-details-section">
                            <h4>Gallery</h4>
                            <!--Carousel Wrapper-->
                            <div id="carousel-thumb"
                                class="carousel slide carousel-fade carousel-thumbnails list-gallery"
                                data-ride="carousel">
                                <!--Slides-->
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}" alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}" alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-3.jpg"
                                            alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-4.jpg"
                                            alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-5.jpg"
                                            alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-6.jpg"
                                            alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-7.jpg"
                                            alt="slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/single-listing/restaurant-8.jpg"
                                            alt="slide">
                                    </div>
                                </div>
                                <!--Controls starts-->
                                <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                    <span class="ion-arrow-left-c" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                    <span class="ion-arrow-right-c" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                <!--Controls ends-->
                                <ol class="carousel-indicators  list-gallery-thumb">
                                    <li data-target="#carousel-thumb" data-slide-to="0"><img
                                            class="img-fluid d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="1"><img
                                            class="img-fluid d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="2"><img
                                            class="img-fluid d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="3"><img
                                            class="img-fluid d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="4"><img
                                            class="img-fluid d-block w-100"
                                            src="{{asset('images/single-listing/restaurant-1.jpg')}}"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="5"><img
                                            class="img-fluid d-block w-100"
                                            src="images/single-listing/restaurant-6.jpg"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="6"><img
                                            class="img-fluid d-block w-100"
                                            src="images/single-listing/restaurant-7.jpg"></li>
                                    <li data-target="#carousel-thumb" data-slide-to="7"><img
                                            class="img-fluid d-block w-100"
                                            src="images/single-listing/restaurant-8.jpg"></li>
                                </ol>
                            </div>
                            <!--/.Carousel Wrapper-->
                        </div>
                    </div>
                    <!--Listing Details ends-->
                </div>
                <!--Sidebar-->
                <div class="col-lg-4 col-md-12">
                    <div class="listing-sidebar">
                        <div class="sidebar-widget">
                            <div id="map"></div>
                            <div class="address">
                                <span class="ion-ios-location"></span>
                                <p>{{$fulladdress}}</p>
                            </div>
                            <div class="address">
                                <span class="ion-ios-telephone"></span>
                                <p>{{$number}}</p>
                            </div>
                            <div class="address">
                                <span class="ion-android-globe"></span>
                                <p>{{$vendor->website}}</p>
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <div class="business-time">
                                <div class="business-title">
                                    <h6><i class="ion-android-alarm-clock"></i> Business Hours</h6>
                                    <span class="float-right">Open Now</span>
                                </div>
                                <ul class="business-hours">
                                    <li class="business-open">
                                        <span class="day">Saturday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">11:00 am</span> - <span class="time">06:00 pm</span>
                                        </div>
                                    </li>
                                    <li class="business-open trend-closed">
                                        <span class="day">Sunday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">Closed</span>
                                        </div>
                                    </li>
                                    <li class="business-open">
                                        <span class="day">Monday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">10:00 am</span> - <span class="time">06:00 pm</span>
                                        </div>
                                    </li>
                                    <li class="business-open">
                                        <span class="day">Tuesday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">10:00 am</span> - <span class="time">06:30 pm</span>
                                        </div>
                                    </li>
                                    <li class="business-open">
                                        <span class="day">Wednesday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">09:00 am</span> - <span class="time">05:00 pm</span>
                                        </div>
                                    </li>
                                    <li class="business-open">
                                        <span class="day">Thursday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">10:00 am</span> - <span class="time">07:00 pm</span>
                                        </div>
                                    </li>
                                    <li class="business-open">
                                        <span class="day">Friday</span>
                                        <div class="atbd_open_close_time">
                                            <span class="time">11:00 am</span> - <span class="time">06:00 pm</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Info ends-->

</div>
<!--Page Wrapper ends-->

@endsection
