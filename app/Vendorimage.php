<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorimage extends Model
{
    /**
    * Get the vendor that owns the image.
    */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
