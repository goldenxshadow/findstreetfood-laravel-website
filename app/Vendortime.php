<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendortime extends Model
{
    /**
    * Get the vendor that owns the specific time record/detail.
    */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
