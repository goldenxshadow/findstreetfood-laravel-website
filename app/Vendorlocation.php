<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorlocation extends Model
{
    /**
    * Get the user that manages the vendor.
    */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
