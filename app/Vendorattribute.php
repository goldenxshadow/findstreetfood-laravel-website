<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorattribute extends Model
{
    /**
    * The vendors that belong to the attribute.
    */
    public function users()
    {
        return $this->belongsToMany('App\Vendor');
    }
}
