<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * Get the user that manages the vendor.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
    /**
    * Get the vendor-location record associated with the vendor.
    */
    public function vendorlocation()
    {
        return $this->hasOne('App\Vendorlocation');
    }

    /**
    * Get the serving-times for the vendor.
    */
    public function vendortimes()
    {
        return $this->hasMany('App\Vendortime');
    }

       /**
    * Get the serving-times for the vendor.
    */
    public function vendorimages()
    {
        return $this->hasMany('App\Vendorimage');
    }

    /**
    * The attributes that belong to the Vendor.
    */
    public function vendorattributes()
    {
        return $this->belongsToMany('App\Vendorattribute');
    }


    


}
